#pragma once
#include "CException.h"
#include <stdio.h>
#include <stdlib.h>
#include <type_traits>

#define DEFAULT_SIZE 16

// Codes d'Erreurs

#define ERREUR_ALLOC 0
#define INDEX_HORS_LISTE 1


template <class T>
class CListe
{

private:
		
	unsigned int uiLISTaille;
	unsigned int uiLISTailleMax;
	T* pLISElement;
	

	void LISVerifierTaille(); 
	
public:

	//-------------------------- CONSTRUCTEURS ET DESTRUCTEUR --------------//

	CListe<T>() : uiLISTaille(0), uiLISTailleMax(DEFAULT_SIZE), pLISElement(nullptr)	{ pLISElement = (T*)malloc(uiLISTailleMax * sizeof(T)); }
	~CListe<T>()																		{ free(pLISElement); }

	//------------------------------- METHODES ------------------------------//

	T& operator[](int iPos) const						{ return *(T*)LISLireElement(iPos); }
	
	void operator*(int coef) {
		if (std::is_fundamental<T>::value) { std::cout <<  "fund" << std::endl; 
		//									for (unsigned int i = 0; i < uiLISTaille; i++) pLISElement[i] *= coef;
		}
		else { std::cout << "not fund" << std::endl; }
	}
	


	void LISAjouterElement(const T& TElement, int uiPos=-1); // par defaut � la fin
	void LISAfficherListe();

	//------------------------------ ACCESSEURS ----------------------------//

	//--------------------- Getters

	unsigned int LISLireTaille() const { return uiLISTaille; }


	/* Erreurs g�r�es en post condtions
	/ Peut prendre des index n�gatifs, comme les listes en python on part de la fin, modulo la taille de la liste*/

	T*	LISLireElement(int iPos) const	//Si iPos trop grand, on renvoie le dernier element
	{
		if (iPos > (int)uiLISTaille) { std::cout << "a\n"; return (pLISElement + LISLireTaille() - 1); }
		else if (iPos <= -1) { std::cout << "b\n"; return (pLISElement + LISLireTaille() + iPos%(int)uiLISTaille); }
		else { std::cout << "c\n"; return (pLISElement + iPos); }
	}

	//------------------- Setters

	void LISModifierElement(unsigned int uiPos, const T& TElement) { 

		if (uiPos > uiLISTaille) {
			throw (CException(INDEX_HORS_LISTE));
		}

		*(pLISElement + uiPos) = TElement; 
	} 

};

//===============================================================================================================================================================================//

/*********************************
* Nom : LISVerifierTaille()
**********************************
* Entr�es : 
* Pr�conditions : 
* Sorties : 
* Postconditions : {uiLISTailleMax > uiLISTaille OU Exception}
***********************************/
template <class T>
void CListe<T>::LISVerifierTaille()
{
	if (LISLireTaille() >= uiLISTailleMax - 1) {

		std::cout << "La liste n'a plus de place. On la r�alloue." << std::endl;

		uiLISTailleMax = uiLISTailleMax * 2;
		void* res = realloc(pLISElement, uiLISTailleMax);

		if (!res) {
			throw (CException(ERREUR_ALLOC));
		}
	}
}

/*********************************
* Nom : LISAjouterElement(const T& TElement, int uiPos)
**********************************
* Entr�es : - TElement : const <<template T&>>
*			- uiPos    : Entier
* Pr�conditions : 
* Sorties : 
* Postconditions : {CListe[uiPos] = TElement}
***********************************/
template <class T>
void CListe<T>::LISAjouterElement(const T& TElement, int uiPos)
{
	// On gere les erreurs telles que : uiPos trop grand, ou uiPos par defaut
	if (uiPos == -1 || uiPos > uiLISTaille) {
		uiPos = LISLireTaille(); // On met l'element  � la derni�re place
	}

	LISVerifierTaille();
	uiLISTaille++;

	for (unsigned int uiBoucle = uiLISTaille; uiBoucle > uiPos; uiBoucle--) {
		pLISElement[uiBoucle] = pLISElement[uiBoucle - 1];
	}

	LISModifierElement(uiPos, TElement);

}

/*********************************
* Nom : LISAfficherListe()
**********************************
* Entr�es :
* Pr�conditions :
* Sorties :
* Postconditions :
***********************************/
template <class T>
void CListe<T>::LISAfficherListe()
{
	std::cout << "[";
	for (unsigned int uiBoucle = 0; uiBoucle < uiLISTaille-1; uiBoucle++) {
		std::cout << pLISElement[uiBoucle] << ",";
	}
	std::cout << pLISElement[uiLISTaille-1];
	std::cout << "]" << std::endl;
}
