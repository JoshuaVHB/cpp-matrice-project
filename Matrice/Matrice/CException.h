#pragma once
#include <stdio.h>
#include <iostream>
#include <stdlib.h>

class CException
{

private:

	unsigned int uiEXCValeur;

public:

	CException(unsigned int uiVal) : uiEXCValeur(uiVal) {}

	void afficher() { std::cout << uiEXCValeur << std::endl; }
};

